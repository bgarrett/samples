 /***************************************************
 
 	jQuery Plugin for duplicating Fields in a fieldset
 
 
 ****************************************************/

(function($){
$.fn.dupFieldset = function(options) {
	
	//stuff goes here
	var defaults = {
		autoGenerate: 0
	};
	
    var options = $.extend(defaults, options);  
	
	return this.each(function(){
		
		
		//Select div and clone read only to duplicate fieldset
		var $fieldset = $(this);
		
		if(options.autoGenerate) newFieldset($fieldset);
		
		$fieldset.find(".add").click(function(){  newFieldset($fieldset); });
		$fieldset.find(".delete").live("click", function(){  $(this).closest("div").remove(); });
		
		 
	});
	
	
	function newFieldset($fieldset){
		
		var $newFieldset = $fieldset.find(".fieldset .read").clone();
		var count = parseInt($fieldset.find(".countfield").val()) + 1;
		
		$fieldset.find(".countfield").val(count);

		
		$newFieldset.attr({
			id: "fieldset" + count	
		});
		
		$newFieldset.removeClass();
		
		$newFieldset.find("input, textarea, select").each(function(){
			$(this).attr({
				name: $(this).attr("name") + count,
				id: $(this).attr("id") + count,
			});
		});
	
		$fieldset.find(".fieldset").append($newFieldset);
	}
			
};

})(jQuery);


/*
	
	Asynchronous calls for data


*/

var model = {};
	
	//Model URL
	model.url = '/maps/online/cfc/features.cfc';
	
	/*
		Returns features of a certain type
		@param type - type of feature return, must be type id
		@param callback - code to run after data has been retrieved
	*/
	model.getFeatureByType = function(type, callback){
		$.ajax({
			url: model.url,
			type: 'get',
			data: {method: 'getFeatureByType', type: type, returnFormat: 'json'},
			success: function(data){callback(data);}
		});
	};
	
	/*
		Returns # of results of features matching a string
		@param term - string to search by
		@param results - # of results to get
		@param callback - code to run after data has been retrieved
	*/
	model.searchFeature = function(term, results, callback){
		$.ajax({
			url: model.url,
			type: 'get',
			dataType: 'json',
			data: {method: 'autocompleteFeature', term: term, results: results, returnFormat: 'json'},
			success: function(data){callback(data);}
		});	
	};
	
	/*
		Returns nearest parking lots to feature
		@param id - id of the feature
		@param callback - code to run after data has been retrieved
	*/
	model.getNearestParkingLots = function(id, callback){
		
		$.ajax({
			url: model.url,
			type: 'get',
			dataType: 'json',
			data: {method: 'getNearestParkingLots', id: id, returnFormat: 'json'},
			success: function(data){callback(data);}
		});	
	};
	
